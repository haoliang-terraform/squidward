const std = @import("std");
const mem = std.mem;
const log = std.log;

const h = @cImport(@cInclude("nvim.h"));

fn dumpBufferImpl(bufnr: i32, outfile: []const u8, start: i32, stop: i32) !bool {
    if (start < 0 or stop < start) return error.InvalidRange;

    const buf = h.buflist_findnr(bufnr);
    if (buf == null) return error.NoSuchBuffer;

    var file = try std.fs.createFileAbsolute(outfile, .{});
    defer file.close();

    // ml_get_buf uses 1-based lnum
    var i = start + 1;
    while (i < stop + 1) : (i += 1) {
        // note: when lnum out of bounds, `???` occurs
        const cline = h.ml_get_buf(buf, i, false);
        const line = mem.span(cline);
        try file.writeAll(line);
        // todo: line break varies
        try file.writeAll("\n");
    }

    return true;
}

export fn cthulhu_dump_buffer(bufnr: i32, outfile: [*:0]const u8, start: i32, stop: i32) bool {
    return dumpBufferImpl(bufnr, mem.span(outfile), start, stop) catch |err| {
        log.err("{}", .{err});
        return false;
    };
}

export fn cthulhu_no_lpl() void {
    h.p_lpl = 0;
}
