local M = {}

local bufdispname = require("infra.bufdispname")

local api = vim.api

local state = {
  had_setup = false,
  ---@type table<number, string>
  givennames = {},
}

function M.setup()
  api.nvim_create_autocmd("tabclosed", {
    callback = function(args)
      -- args: {buf: number, event: string, file: numeric-string, id: number, match = numeric-string}
      local tabpage = assert(tonumber(args.file))
      -- todo: what if we are closing the last one tabpage?
      state.givennames[tabpage] = nil
    end,
  })

  state.had_setup = true
end

-- possible operations:
-- * (nil, nil)
-- * (number, nil)
-- * (nil, string)
-- * (number, string)
---@param newname string?
---@param tabpage number?
function M.rename(newname, tabpage)
  if tabpage == nil or tabpage == 0 then tabpage = api.nvim_get_current_tabpage() end

  state.givennames[tabpage] = newname
  vim.cmd.redrawtabline()
end

do
  ---@param tabpage number
  ---@return string
  local function resolve_tab_title(tabpage)
    local givenname = state.givennames[tabpage]
    if givenname ~= nil then return givenname end

    local win_id = api.nvim_tabpage_get_win(tabpage)
    local bufnr = api.nvim_win_get_buf(win_id)
    local bufname = api.nvim_buf_get_name(bufnr)
    local buftype = api.nvim_buf_get_option(bufnr, "buftype")

    -- regular file
    if buftype == "" then
      return bufdispname.blank(bufnr, bufname) or bufdispname.stem(bufnr, bufname)
    else
      return bufdispname.proto_abbr(bufnr, bufname) or bufdispname.filetype_abbr(bufnr, bufname) or bufname
    end
  end

  ---@return string
  function M.render()
    -- %#TabLine#%1T init %T%#TabLineSel#%2T tabline %T%#TabLineFill#%=%#TabLine#%999XX

    assert(state.had_setup)

    local parts = {}
    do
      local tabs = api.nvim_list_tabpages()
      -- only one tab, no need to set
      if #tabs == 1 then return "" end
      local cur_tabpage = api.nvim_get_current_tabpage()
      for i = 1, #tabs do
        local tabpage = tabs[i]
        -- todo: get these list in one api query
        local title = resolve_tab_title(tabpage)
        local focus = tabpage == cur_tabpage and "%#TabLineSel#" or "%#TabLine#"
        table.insert(parts, string.format([[%s%%%dT%s]], focus, i, title))
      end
      if not "i need a button" then table.insert(parts, "%T%#TabLineFill#%=%#TabLine#%999XX") end
    end

    return table.concat(parts, " ")
  end
end

return M
