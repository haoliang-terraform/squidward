-- inspired by tmux's <prefix>-z
--
-- design choices
-- * fullscreen, no tabline, statusline, cmdline
-- * window will be closed on :sp, :vs, :help
--

local api = vim.api

local state = { win_id = nil }

-- toggle zoom in/out a window, intended not for buffer
return function()
  if state.win_id ~= nil then
    assert(state.win_id == api.nvim_get_current_win())
    api.nvim_win_close(state.win_id, false)
    state.win_id = nil
  else
    local bufnr = api.nvim_get_current_buf()
    -- show in fullscreen
    state.win_id = api.nvim_open_win(bufnr, true, { relative = "editor", row = 0, col = 0, width = vim.o.columns, height = vim.o.lines })
    api.nvim_win_set_option(state.win_id, "winhl", "NormalFloat:Normal")
    api.nvim_create_autocmd("WinLeave", {
      once = true,
      callback = function()
        api.nvim_win_close(state.win_id, false)
        state.win_id = nil
      end,
    })
  end
end
