local M = {}

local api = vim.api
local profiles = require("profiles")

---@class batteries.PluginRtps
---@field pre string
---@field post string?
---@field plugin string?
---
---@class batteries.Facts
---@field plugin_rtps table<string, batteries.PluginRtps>
---@field native_rtps string[]
---@field builtin_plugins string[]
local Facts = {}
---@class batteries.Facts.filesystem
---@field root string
---@field data string
---@field treesitter_libdir string
Facts.filesystem = {}

---@type batteries.Facts
local facts = (function(native_rtps)
  local base
  do
    local path = string.format("%s/%s", vim.fn.stdpath("data"), "batteries.json")
    local file = assert(io.open(path, "r"))
    local data = file:read("*a")
    file:close()
    base = vim.json.decode(data)
  end
  base.native_rtps = vim.split(native_rtps, ",", { plain = true })
  do
    local root = vim.fn.stdpath("config")
    local function pathto(...) return table.concat({ root, ... }, "/") end
    base.plugin_rtps.cthulhu = { pre = pathto("cthulhu") }
    base.plugin_rtps.natives = { pre = pathto("natives") }
    base.plugin_rtps.wiki = { pre = pathto("hybrids/wiki") }
  end
  do
    local root = vim.env.VIMRUNTIME .. "/plugin"
    local files = { "man.lua", "rplugin.vim", "shada.vim" }
    base.builtin_plugins = vim.tbl_map(function(el) return root .. "/" .. el end, files)
  end
  return base
end)(vim.go.rtp)

--order matters
local profile_plugins = {
  { "base", { "cthulhu", "natives" } },
  { "wiki", { "wiki" } },
  { "joy", { "guwen" } },
  { "git", { "fugitive", "gv" } },
  { "code", { "indent-object", "commentary", "easy-align", "surround" } },
  { "lsp", { "lspconfig" } },
}

local function valid_native_rtp(rtp)
  -- no system-wide xdg config
  if vim.startswith(rtp, "/etc/xdg/nvim") then return false end
  -- no more vim packages
  if string.find(rtp, "/site") then return false end
  return true
end

function M.install()
  local rtps = {}
  do
    local pre, post = {}, {}
    for _, defn in pairs(profile_plugins) do
      local profile, plugins = unpack(defn)
      if profiles.has(profile) then
        for _, plugin in pairs(plugins) do
          local rtp = facts.plugin_rtps[plugin]
          if rtp ~= nil then
            table.insert(pre, rtp.pre)
            table.insert(post, rtp.post)
          end
        end
      end
    end

    -- order:
    -- * stdpath('config')
    -- * batteries.plugins.pre
    -- * vim.default.{pre,after}
    -- * batteries.plugins.after
    local tip = vim.fn.stdpath("config")
    table.insert(rtps, tip)
    table.insert(rtps, facts.filesystem.treesitter_libdir)
    -- pre's
    for _, path in ipairs(pre) do
      table.insert(rtps, path)
    end
    for _, path in pairs(facts.native_rtps) do
      if path ~= tip and valid_native_rtp(path) then table.insert(rtps, path) end
    end
    -- post's
    for _, path in ipairs(post) do
      table.insert(rtps, path)
    end
  end

  api.nvim_set_option("rtp", table.concat(rtps, ","))
end

-- todo: honor after/plugin
function M.load_rtp_plugins()
  for _, defn in pairs(profile_plugins) do
    local profile, plugins = unpack(defn)
    if profiles.has(profile) then
      for _, plugin in pairs(plugins) do
        local plugin_file = facts.plugin_rtps[plugin].plugin
        if plugin_file then vim.cmd.source(plugin_file) end
      end
    end
  end

  for _, plugin_file in pairs(facts.builtin_plugins) do
    vim.cmd.source(plugin_file)
  end
end

function M.datadir(ent) return string.format("%s/%s", facts.filesystem.data, ent) end

---@param plugin string
---@return boolean
function M.has(plugin)
  for _, defn in pairs(profile_plugins) do
    local profile, plugins = unpack(defn)
    if vim.tbl_contains(plugins, plugin) and profiles.has(profile) then return true end
  end
  return false
end

return M
