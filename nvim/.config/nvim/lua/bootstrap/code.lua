local batteries = require("batteries")

local g = vim.g

if batteries.has("ultisnips") then
  g.UltiSnipsExpandTrigger = "<tab>"
  g.UltiSnipsJumpForwardTrigger = "<tab>"
  g.UltiSnipsJumpBackwardTrigger = "<s-tab>"
  g.UltiSnipsEditSplit = "vertical"
  g.snips_author = "haoliang"
end

if batteries.has("easy-align") then -- easy-align
  g.easy_align_ignore_groups = { "Comment", "String" } -- it's not working!
  vim.keymap.set("v", "<enter>", "<Plug>(EasyAlign)")
end

do -- man
  g.ft_man_folding_enable = true
end

if batteries.has("commentary") then -- commentary
  vim.keymap.set("v", "gc", "<Plug>Commentary")
  vim.keymap.set("n", "gc", "<Plug>CommentaryLine")
end

if batteries.has("surround") then g.surround_no_insert_mappings = 1 end
