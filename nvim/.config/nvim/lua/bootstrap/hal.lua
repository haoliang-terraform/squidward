local jelly = require("infra.jellyfish")("bootstrap.hal")

local api = vim.api

local function nmap(lhs, rhs)
  local rhs_type = type(rhs)
  if rhs_type == "function" then
    vim.api.nvim_set_keymap("n", lhs, "", { silent = false, noremap = true, callback = rhs })
  elseif rhs_type == "string" then
    vim.api.nvim_set_keymap("n", lhs, rhs, { silent = false, noremap = true })
  else
    error("unreachable: unpexpected rhs type")
  end
end
local usercmd = api.nvim_create_user_command
local function vmap_silent(lhs, rhs) vim.api.nvim_set_keymap("v", lhs, rhs, { silent = true }) end

do
  local olds = require("olds")
  if olds.setup("/run/user/1000/redis.sock") then
    olds.auto()
  else
    jelly.info("olds.setup failed")
  end
end

require("fond.setup")()

vim.ui.select = function(...) return require("display_menu")(...) end

api.nvim_set_option("statusline", [[%!v:lua.require'statusline'.render()]])

do
  require("tabline").setup()
  api.nvim_set_option("tabline", [[%!v:lua.require'tabline'.render()]])
  usercmd("RenameTabpage", function(args)
    local nargs = #args.fargs
    if nargs == 0 then
      require("tabline").rename()
    else
      require("tabline").rename(args.args)
    end
  end, { nargs = "?" })
end

do
  nmap([[<leader>q]], function() require("qltoggle").toggle_qflist() end)
  nmap([[<leader>l]], function() require("qltoggle").toggle_loclist() end)
  nmap([[<leader>`]], function() require("popupshell").floatwin() end)
end

do
  local comp_bufname = function()
    local name = vim.fn.expand("%:t")
    if name == "[Command Line]" and vim.bo.filetype == "vim" then name = vim.fn.expand("#:t") end
    return { name }
  end

  usercmd("Touch", function(args) require("infra.coreutils").relative_touch(args.args) end, { nargs = 1, complete = comp_bufname })
  usercmd("Mv", function(args) require("infra.coreutils").rename_filebuf(0, args.args) end, { nargs = 1, complete = comp_bufname })
  usercmd("Rm", function() require("infra.coreutils").rm_filebuf(0) end, { nargs = 0 })
  usercmd("Mkdir", function(args) require("infra.coreutils").relative_mkdir(args.args) end, { nargs = 1 })
end

do
  vmap_silent([[*]], [[:lua require"infra.vsel".search_forward()<cr>]])
  vmap_silent([[#]], [[:lua require"infra.vsel".search_backward()<cr>]])
  vmap_silent([[<leader>s]], [[:lua require"infra.vsel".substitute()<cr>]])
end

do
  nmap([[<leader>/]], function() require("grep").rg.input("repo") end)
  vmap_silent([[<leader>/]], [[:lua require("grep").rg.vsel('repo')<cr>]])
  usercmd("Todo", function() require("grep").rg.text("repo", [[\btodo@?]]) end, { nargs = 0 })
end

for i = 1, 9 do
  nmap(string.format([['%d]], i), string.format([[<cmd>lua require'winjump'.to(%d)<cr>]], i))
end

do
  nmap("-", function() require("kite").fly() end)
  nmap("_", function() require("kite").land() end)
  nmap("[k", function() require("kite").rhs_open_sibling_file("prev") end)
  nmap("]k", function() require("kite").rhs_open_sibling_file("next") end)
end

do
  nmap([[<leader>s]], function() require("fond").files() end)
  nmap([[<leader>g]], function() require("fond").tracked() end)
  nmap([[<leader>b]], function() require("fond").buffers() end)
  nmap([[<leader>u]], function() require("fond").modified() end)
  nmap([[<leader>m]], function() require("fond").olds() end)
  nmap([[<leader>f]], function() require("fond").siblings() end)
  nmap([[<leader>d]], function() require("fond").symbols() end)
  nmap([[<leader>w]], function() require("fond").windows() end)
  -- fresh version
  nmap([[\s]], function() require("fond").files(false) end)
  nmap([[\g]], function() require("fond").tracked(false) end)
  nmap([[\m]], function() require("fond").olds(false) end)
  nmap([[\f]], function() require("fond").siblings(false) end)
  nmap([[\d]], function() require("fond").symbols(false) end)
end

do
  vim.keymap.set("i", "<c-space>", function() require("parrot").expand() end)
  vim.keymap.set({ "n", "v", "x" }, "<tab>", function() require("parrot").goto_next() end)
end

nmap("<c-w>z", function() require("winzoom")() end)

usercmd("Nag", function(args)
  local meth = args.args == "" and "tab" or args.args
  assert(require("nag")[meth], "nag has no such split cmd")()
end, { nargs = "*", range = true, complete = function() return { "tab", "split", "vsplit" } end })
usercmd("Resize", function() require("winresize")() end, { nargs = 0 })
usercmd("Pstree", function(args) require("pstree").run(args.fargs) end, { nargs = "*" })
usercmd("Punctuate", function() require("punctconv").multiline_vsel() end, { nargs = 0, range = true })
usercmd("W", function() require("sudo_write")() end, { nargs = 0 })

nmap("<leader>.", function() require("reveal")(nil, true) end)

_G.inspect = function(...) require("inspect").popup(...) end
