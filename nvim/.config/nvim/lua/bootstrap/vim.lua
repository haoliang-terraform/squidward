local api = vim.api

local function resolve_clipboard_provider()
  -- x11
  if vim.env.DISPLAY ~= nil then
    return {
      name = "xsel",
      copy = {
        ["+"] = { "xsel", "--nodetach", "-ib" },
        ["*"] = { "xsel", "--nodetach", "-ip" },
      },
      paste = {
        ["+"] = { "xsel", "-ob" },
        ["*"] = { "xsel", "-op" },
      },
      cache_enabled = true,
    }
  end

  -- tmux
  if vim.env.TMUX ~= nil then
    return {
      name = "tmux",
      copy = {
        ["+"] = { "tmux", "load-buffer", "-" },
        ["*"] = { "tmux", "load-buffer", "-" },
      },
      paste = {
        ["+"] = { "tmux", "save-buffer", "-" },
        ["*"] = { "tmux", "save-buffer", "-" },
      },
      cache_enabled = true,
    }
  end

  -- no luck left
  return false
end

do -- vim options
  local gv = vim.g
  local go = vim.go
  local lo = vim.o
  local cmd = vim.cmd

  cmd.syntax("off")
  cmd.filetype("indent", "off")
  cmd.filetype("plugin", "off")
  go.loadplugins = false

  gv.mapleader = " "

  -- backup && history
  go.backup = false
  go.writebackup = false
  lo.swapfile = false
  lo.undofile = false
  go.undolevels = 200
  go.undoreload = 100
  go.history = 100

  -- search
  go.magic = true
  go.ignorecase = true
  go.smartcase = true
  go.hlsearch = true
  go.incsearch = true

  -- encoding
  go.encoding = "utf8"
  lo.fileencoding = "utf-8"
  go.fileencodings = "utf-8"
  lo.fileformat = "unix"
  go.fileformats = "unix"

  -- complete
  go.completeopt = "menuone,noinsert"
  go.wildmode = "full:lastused"
  go.wildmenu = true
  go.more = true
  go.suffixes = ".un~,.bak,~,.swp,.log,.data"
  go.wildignore = table.concat({
    go.wildignore,
    "*~,*.bak,*.un~,tags", -- for vim
    --"*/.git/*" -- disable for fugitive
    "*.data",
    "*.jpg,*.gif,*.png,*.psd",
    "*/__pycache__/*,*/venv*/*", -- for python
    "*/node_modules/*", -- for javascript
    "*/zig-cache/*,*/zig-out/*", -- for zig
  }, ",")

  -- ui
  lo.colorcolumn = ""
  cmd.colorscheme("doodlebob")
  lo.number = false
  lo.relativenumber = true
  lo.numberwidth = 1
  lo.signcolumn = "no"
  go.cmdheight = 1
  go.laststatus = 3
  lo.foldenable = false

  -- tab & space
  lo.tabstop = 4
  lo.softtabstop = 4
  lo.shiftwidth = 4
  lo.expandtab = true
  go.shiftround = true

  -- line display
  lo.wrap = false
  lo.linebreak = true
  lo.textwidth = 78
  go.linespace = 5
  lo.formatoptions = "tcqn2vlB1j"
  lo.list = false

  -- misc
  go.scrolloff = 0
  go.shada = [[:100,@100,@100,'0,f0,/0,<0,s10,h]]
  go.mouse = ""
  go.keywordprg = ":help"
  lo.modeline = true
  go.modelines = 5
  go.showmode = false
  go.shortmess = "filnxtToOF" .. "aoOsTIcF"
  go.autochdir = false
  go.sidescroll = 20
  go.autoread = false
  go.hidden = true
  go.showmatch = true
  go.visualbell = true
  go.splitbelow = true
  go.splitright = true
  go.inccommand = ""
  go.updatetime = 2000 -- for CursorHold
  go.title = true
  go.titlelen = 10
  go.titlestring = [[vi:%{expand("%:t:r")}]]
  gv.clipboard = resolve_clipboard_provider()
end

do -- map
  local function t(lhs, rhs) vim.keymap.set("t", lhs, rhs, { noremap = true }) end
  local function n(lhs, rhs) vim.keymap.set("n", lhs, rhs, { noremap = true }) end
  local function c(lhs, rhs) vim.keymap.set("c", lhs, rhs, { noremap = true }) end
  local function i(lhs, rhs) vim.keymap.set("i", lhs, rhs, { noremap = true }) end

  -- disable default
  n("<space>", "<nop>")
  n("s", "<nop>")

  -- bash-ish
  c("<c-a>", "<home>")
  c("<c-e>", "<end>")
  c("<c-p>", "<up>")
  c("<c-n>", "<down>")

  -- switch windows
  n("<c-l>", "<c-w>l")
  n("<c-h>", "<c-w>h")
  n("<c-j>", "<c-w>j")
  n("<c-k>", "<c-w>k")

  -- better default
  n("0", "^")
  n("^", "0")
  n("Y", "y$")

  -- pairs
  n("[q", "<cmd>cprev<cr>")
  n("]q", "<cmd>cnext<cr>")
  n("[c", "<cmd>lprev<cr>")
  n("]c", "<cmd>lnext<cr>")
  n("[b", "<cmd>bprev<cr>")
  n("]b", "<cmd>bnext<cr>")
  n("[a", "<cmd>next<cr>")
  n("]a", "<cmd>prev<cr>")

  -- <c-g>u for new undo step
  i("<c-l>", "<c-g>u<del>")
  i("<c-a>", "<c-g>u<home>")
  i("<c-e>", "<c-g>u<end>")

  -- changelist
  n("<c-;>", "g;")
  n("<c-,>", "g,")

  n([[\\]], [[<cmd>call setreg('/', '')<cr>]])
  n("<leader>a", "<cmd>e #<cr>")
  n("<leader>v", "<c-v>")

  t("<c-]>", [[<C-\><C-n>]])
end

do -- cmd
  local function c(name, action) vim.api.nvim_create_user_command(name, action, { nargs = 0 }) end

  c("Ts", [[%s/\s\+$//e]]) -- 尾部多余空白符
  c("Tl", [[%s/^\s*\n\s*$//e]]) -- 多余空行
  c("Tm", [[%s/\r//e]]) -- windows
  c("Tt", [[%s/\t/    /ge]]) -- 空格*4
  c("Hd", [[/\v[<>=|]{7}]]) -- git diff
end

do -- autocmd
  api.nvim_create_autocmd("StdinReadPre", {
    desc = "make stdin buffer disposable",
    callback = function(event)
      local bo = vim.bo[event.buf]
      -- same as scratch-buffer
      bo.buftype = "nofile"
      bo.bufhidden = "hide"
      bo.swapfile = false
    end,
  })

  api.nvim_create_autocmd("WinClosed", {
    desc = "reasonable focus change",
    callback = function(args)
      -- do nothing when closing an unfocused window
      if api.nvim_get_current_win() ~= tonumber(args.match) then return end
      require("infra.ex")("wincmd", "p")
    end,
  })

  api.nvim_create_autocmd("TextYankPost", {
    desc = "flash the text being copied",
    callback = function() vim.highlight.on_yank({ higroup = "IncSearch", timeout = 150, on_visual = true }) end,
  })
end

_G.api = vim.api
