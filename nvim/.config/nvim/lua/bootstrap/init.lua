local profiles = require("profiles")

local has_ran = false

return function()
  assert(not has_ran, "should only bootstrap once")
  has_ran = true

  require("bootstrap.vim")
  require("bootstrap.langspecs")
  require("bootstrap.hal")
  if profiles.has("code") then require("bootstrap.code") end
  if profiles.has("lsp") then require("bootstrap.lsp") end
  if profiles.has("joy") then require("bootstrap.joy") end
  if profiles.has("wiki") then require("wiki.setup")() end
end
