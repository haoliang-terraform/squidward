local api = vim.api
local ex = require("infra.ex")
local profiles = require("profiles")

local bufmap = function(bufnr)
  local function noremap(mode, lhs, rhs)
    local rhs_type = type(rhs)
    if rhs_type == "string" then
      api.nvim_buf_set_keymap(bufnr, mode, lhs, rhs, { noremap = true })
    elseif rhs_type == "function" then
      api.nvim_buf_set_keymap(bufnr, mode, lhs, "", { noremap = true, callback = rhs })
    else
      error("unsupported rhs type")
    end
  end

  return {
    v = function(lhs, rhs) noremap("v", lhs, rhs) end,
    n = function(lhs, rhs) noremap("n", lhs, rhs) end,
  }
end

---@diagnostic disable: unused-local

---@type {[string]: {vim: fun(bufnr: number), treesitter?: fun(bufnr: number), lsp?: fun(bufnr: number)}}
local buf_specs = {}
do
  local function adds(new)
    for k, v in pairs(new) do
      if buf_specs[k] ~= nil then error(string.format("%s already exists in buf_specs", k)) end
      buf_specs[k] = v
    end
  end
  -- langs
  adds({
    lua = {
      vim = function(bufnr)
        local bo, bm = vim.bo[bufnr], bufmap(bufnr)
        bo.commentstring = [[-- %s]]
        bo.tabstop = 2
        bo.softtabstop = 2
        bo.shiftwidth = 2
        bo.expandtab = true
        bm.v("K", [[:lua require("help").nvim()<cr>]])
        bm.n("gq", function() require("formatter").run() end)
        bm.n("<leader>r", function() require("windmill").autorun() end)
      end,
      treesitter = function(bufnr)
        vim.treesitter.start(bufnr, "lua")
        -- squirrel
        do
          local bm = bufmap(bufnr)
          bm.n("gx", function() require("squirrel.docgen.lua")() end)
          bm.v("g>", [[:lua require'squirrel.veil'.cover('lua')<cr>]])
          bm.n("vin", function() require("squirrel.incsel")() end)
          require("squirrel.jumps").attach("lua")
          require("squirrel.folding").attach("lua")
        end
      end,
    },
    python = {
      vim = function(bufnr)
        local bo, bm = vim.bo[bufnr], bufmap(bufnr)
        bo.suffixesadd = ".py"
        bo.comments = [[b:#,fb:-]]
        bo.commentstring = [[# %s]]
        bm.n("gq", function() require("formatter").run() end)
        bm.n("<leader>r", function() require("windmill").autorun() end)
      end,
      treesitter = function(bufnr)
        vim.treesitter.start(bufnr, "python")
        -- squirrel
        do
          local bm = bufmap(bufnr)
          bm.n("<leader>i", function() require("squirrel.imports")() end)
          bm.n("vin", function() require("squirrel.incsel")() end)
          require("squirrel.folding").attach("python")
        end
      end,
    },
    zig = {
      vim = function(bufnr)
        local bo, bm = vim.bo[bufnr], bufmap(bufnr)
        bo.suffixesadd = ".zig"
        bo.commentstring = "// %s"
        bm.n("gq", function() require("formatter").run() end)
        bm.n("<leader>r", function() require("windmill").autorun() end)
      end,
      treesitter = function(bufnr)
        vim.treesitter.start(bufnr, "zig")
        -- squirrel
        do
          local bm = bufmap(bufnr)
          bm.v("g>", [[:lua require'squirrel.veil'.cover('zig')<cr>]])
          bm.n("vin", function() require("squirrel.incsel")() end)

          require("squirrel.jumps").attach("zig")
          require("squirrel.folding").attach("zig")
        end
      end,
    },
    bash = {
      vim = function(bufnr)
        local bo, bm = vim.bo[bufnr], bufmap(bufnr)
        bo.suffixesadd = ".sh"
        bo.comments = [[b:#,fb:-]]
        bo.commentstring = [[# %s]]
        bm.n("<leader>r", function() require("windmill").autorun() end)
      end,
      treesitter = function(bufnr)
        vim.treesitter.start(bufnr, "bash")
        -- squirrel
        do
          local bm = bufmap(bufnr)
          bm.v("g>", [[:lua require'squirrel.veil'.cover('sh')<cr>]])
        end
      end,
    },
    c = {
      vim = function(bufnr)
        local bo, bm = vim.bo[bufnr], bufmap(bufnr)
        bo.suffixesadd = ".c"
        -- todo: &comments
        bo.commentstring = [[// %s]]
        bo.expandtab = true
        bo.cindent = true
        bm.n("gq", function() require("formatter").run() end)
        bm.n("<leader>r", function() require("windmill").autorun() end)
      end,
      treesitter = function(bufnr)
        vim.treesitter.start(bufnr, "c")
        -- squirrel
        do
          local bm = bufmap(bufnr)
          bm.n("gq", function() vim.lsp.buf.format({ async = true }) end)
          bm.v("g>", [[:lua require'squirrel.veil'.cover('c')<cr>]])
          bm.n("vin", function() require("squirrel.incsel")() end)
          require("squirrel.folding").attach("c")
        end
      end,
    },
    go = {
      vim = function(bufnr)
        local bo, bm = vim.bo[bufnr], bufmap(bufnr)
        bo.suffixesadd = ".go"
        bo.commentstring = [[// %s]]
        bo.expandtab = false
        bm.n("gq", function() require("formatter").run() end)
        bm.n("<leader>r", function() require("windmill").autorun() end)
      end,
      treesitter = function(bufnr)
        vim.treesitter.start(bufnr, "go")
        -- squirrel
        do
          local bm = bufmap(bufnr)
          bm.n("<leader>i", function() require("squirrel.imports")() end)
          bm.v("g>", [[:lua require'squirrel.veil'.cover('go')<cr>]])
          bm.n("vin", function() require("squirrel.incsel")() end)
          bm.n("gx", function() require("squirrel.docgen.go")() end)
          require("squirrel.folding").attach("go")
        end
      end,
    },
    php = {
      vim = function(bufnr)
        local bo, bm = vim.bo[bufnr], bufmap(bufnr)
        bo.comments = [[s1:/*,mb:*,ex:*/,://,:#]]
        bo.commentstring = [[// %s]]
        bo.suffixesadd = ".php"
        -- php namespace, not fully support psr-0, psr-4
        --setl includeexpr=substitute(substitute(substitute(v:fname,';','','g'),'^\\','',''),'\\','\/','g')
        -- `yii => yii2`
        --bo.includeexpr = [[substitute(substitute(substitute(substitute(v:fname,';','','g'),'^\\','',''),'\\','\/','g'),'yii','yii2','')]]
        bm.n("<leader>r", function() require("windmill").autorun() end)
      end,
      treesitter = function(bufnr) vim.treesitter.start(bufnr, "php") end,
    },
    fennel = {
      vim = function(bufnr)
        local bo, bm = vim.bo[bufnr], bufmap(bufnr)
        bo.commentstring = [[;;\ %s]]
        bo.suffixesadd = ".fnl"
        bm.n("<leader>p", function() require("windmill").preview_fennel() end)
        bm.v("K", function() require("help").luaref() end)
        bm.n("<leader>r", function() require("windmill").autorun() end)
      end,
      -- treesitter = function(bufnr) vim.treesitter.start(bufnr, "php") end,
    },
  })
  -- misc
  adds({
    json = {
      vim = function(bufnr)
        local bm = bufmap(bufnr)
        bm.n("gq", "<cmd>%! jq .<cr>")
      end,
      treesitter = function(bufnr)
        vim.treesitter.start(bufnr, "json")
        -- squirrel
        do
          require("squirrel.folding").attach("json")
        end
      end,
    },
    git = {
      vim = function(bufnr)
        local bo = vim.bo[bufnr]
        bo.syntax = "git"
        ex("runtime syntax/diff.vim syntax/git.vim")
      end,
    },
    gitcommit = {
      vim = function(bufnr)
        local bo = vim.bo[bufnr]
        bo.syntax = "gitcommit"
        ex("runtime syntax/gitcommit.vim")
      end,
    },
    fugitive = {
      vim = function(bufnr)
        local bo, bm = vim.bo[bufnr], bufmap(bufnr)
        bo.syntax = "fugitive"
        ex("runtime syntax/fugitive.vim")
        bm.n("gw", [[<cmd>tab Git commit -v<cr>]])
      end,
    },
    GV = {
      vim = function(bufnr)
        local bo = vim.bo[bufnr]
        bo.expandtab = false
      end,
    },
    help = {
      vim = function(bufnr)
        -- todo: nvim 0.9 vimdoc
        local bo = vim.bo[bufnr]
        bo.bufhidden = "wipe"
        bo.keywordprg = ":help"
      end,
    },
    man = {
      vim = function(bufnr)
        local bm = bufmap(bufnr)
        bm.n("K", [[<cmd>Man<cr>]])
        bm.n("q", [[<cmd>quit<cr>]])
      end,
    },
    qf = {
      vim = function(bufnr)
        local bo, bm = vim.bo[bufnr], bufmap(bufnr)
        -- todo: set stausline<
        bo.syntax = "qf"
        ex("runtime syntax/qf.vim")
        -- diagnositcs can be long
        bm.n("j", "gj")
        bm.n("k", "gk")
        bm.v("j", "gj")
        bm.v("j", "gk")
        bm.n("q", [[<cmd>q<cr>]])
        bm.n("<c-[>", [[<cmd>q<cr>]])
      end,
    },
    make = {
      vim = function(bufnr)
        local bo = vim.bo[bufnr]
        bo.expandtab = false
      end,
    },
    wiki = {
      vim = function(bufnr) ex("runtime syntax/wiki.vim") end,
    },
    sshconfig = {
      vim = function(bufnr) vim.bo[bufnr].commentstring = "# %s" end,
    },
  })
end

local win_specs = {
  lua = function(bufnr, win_id) vim.wo[win_id].list = true end,
  git = function(bufnr, win_id) vim.wo[win_id].list = false end,
  help = function(bufnr, win_id) vim.wo[win_id].conceallevel = 0 end,
  qf = function(bufnr, win_id) vim.wo[win_id].wrap = true end,
  wiki = function(bufnr, win_id) vim.wo[win_id].conceallevel = 3 end,
  man = function(bufnr, win_id)
    local wo = vim.wo[win_id]
    wo.number = false
    wo.relativenumber = false
  end,
}

local aug = api.nvim_create_augroup("langspec", {})

do -- main
  api.nvim_create_autocmd("FileType", {
    group = aug,
    desc = "per lang spec: buf-local",
    callback = function(args)
      local bufnr, ft = args.buf, args.match
      local spec = buf_specs[ft]
      if spec == nil then return end
      spec.vim(bufnr)
      if profiles.has("treesitter") and spec.treesitter then spec.treesitter(bufnr) end
      if profiles.has("lsp") and spec.lsp then spec.lsp() end
    end,
  })

  -- todo: prevent duplicate running
  api.nvim_create_autocmd({ "WinNew", "BufWinEnter" }, {
    group = aug,
    desc = "per lang spec: win-local",
    callback = function(args)
      local bufnr, win_id = args.buf, api.nvim_get_current_win()
      local ft = vim.bo[bufnr].filetype
      local spec = win_specs[ft]
      if spec == nil then return end
      spec(bufnr, win_id)
    end,
  })
end
