local ran = false

return function()
  assert(not ran)
  ran = true

  vim.filetype.add({
    extension = {
      wiki = function(path, bufnr)
        local _ = path
        local bo = vim.bo[bufnr]
        local ex = require("infra.ex")
        local api = vim.api

        bo.syntax = "wiki"
        bo.suffixesadd = ".wiki"
        bo.textwidth = 90
        bo.formatoptions = bo.formatoptions .. "]"
        bo.shiftwidth = 2
        bo.tabstop = 2
        bo.softtabstop = 2

        api.nvim_buf_set_keymap(bufnr, "n", "<cr>", "", {
          noremap = true,
          callback = function() require("wiki").edit_link() end,
        })
        api.nvim_buf_set_keymap(bufnr, "n", "<c-]>", "", {
          noremap = true,
          callback = function()
            ex("rightbelow vsplit")
            require("wiki").edit_link()
          end,
        })

        return "wiki"
      end,
    },
  })
end
