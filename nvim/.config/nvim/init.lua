require("profiles").init()
require("batteries").install()

require("bootstrap")()

assert(not vim.go.loadplugins) -- so that no need to turn off {netrw,tar,zip,tutor,vimball,...} explictly
require("batteries").load_rtp_plugins()
