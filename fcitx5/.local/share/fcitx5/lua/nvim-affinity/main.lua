-- partialy stolen from https://zenn.dev/anyakichi/articles/a3aab8d80994d1
-- see src/addonloader/luaaddonstate.cpp, /usr/include/Fcitx5

local fcitx = require("fcitx")

--this fn have to be global!
--if the fn returns true, event.filterAndAccept() will be called (but i had not figured out what does this fn mean)
---@alias KeyEventWatcher fun(sym: number, state: number, is_release: boolean) : boolean?

fcitx.watchEvent(fcitx.EventType.KeyEvent, "nvim_affinity_handle")

---@type KeyEventWatcher
---@diagnostic disable-next-line
function nvim_affinity_handle(sym, state, is_release)
  local is_escape = (sym == 65307 and state == 0) or (sym == 91 and state == 4)
  if not (is_escape and not is_release) then return end

  if fcitx.currentInputMethod() ~= "rime" then return end

  -- lua addon will block the fcitx5 process including the dbus of rime,
  -- thus we should use either `--timeout=0` or `--expect-reply=no` here.
  --
  -- see also: busctl introspect --user org.fcitx.Fcitx5 /rime
  --os.execute([[busctl call --user --expect-reply=no org.fcitx.Fcitx5 /rime org.fcitx.Fcitx.Rime1 SetAsciiMode b 1]])

  os.execute([[rimeascii &]])
end
