const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    const mode = b.standardReleaseOptions();
    const strip = mode != .Debug;
    const output_dir = b.pathJoin(&.{ b.env_map.get("HOME").?, "bin" });

    inline for (.{ "playvideos", "backlight" }) |name| {
        const exe = b.addExecutable(name, "src/" ++ name ++ ".zig");
        exe.setBuildMode(mode);
        exe.setOutputDir(output_dir);
        exe.single_threaded = true;
        exe.strip = strip;
        exe.install();
    }

    {
        const exe = b.addExecutable("rimeascii", "src/rimeascii.zig");
        exe.setBuildMode(mode);
        exe.setOutputDir(output_dir);
        exe.strip = strip;
        exe.linkLibC();
        exe.linkSystemLibrary("dbus-1");
        exe.install();
    }

    {
        const exe_tests = b.addTest("src/test.zig");
        exe_tests.linkLibC();
        exe_tests.linkSystemLibrary("dbus-1");
        exe_tests.setBuildMode(mode);

        const test_step = b.step("test", "Run unit tests");
        test_step.dependOn(&exe_tests.step);
    }
}
